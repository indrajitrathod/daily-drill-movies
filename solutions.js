const movies = require('./moviesData');

//Q1. Find all the movies with total earnings more than $500M. 
const moviesMorethan500M = (movies) => {
    const result = Object.entries(movies).filter(([name, details]) => {
        return +details.totalEarnings.replace('$', '').slice(0, 3) > 500;
    }).map(([name, details]) => {
        return { [name]: details };
    });
    return result;
}

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const morethanThreeOscarAnd500M = (movies) => {
    const result = Object.entries(movies).filter(([name, details]) => {
        return (+details.totalEarnings.replace('$', '').slice(0, 3) > 500) && (details.oscarNominations > 3);
    }).map(([name, details]) => {
        return { [name]: details };
    });
    return result;
}

//Q.3 Find all movies of the actor "Leonardo Dicaprio".
const leonardoMovies = (movies) => {
    const result = Object.entries(movies).filter(([name, details]) => {
        return details.actors.join(",").toLowerCase().includes('leonardo dicaprio');
    }).map(([name, details]) => {
        return { [name]: details };
    });
    return result;
}

//Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.
const sortMoviesonRating = (movies) => {
    const result = Object.entries(movies).sort(([movieA, movieADetails], [movieB, movieBDetails]) => {
        if(movieBDetails.imdbRating === movieADetails.imdbRating){
            return +movieBDetails.totalEarnings.replace('$', '').slice(0, 3) - +movieADetails.totalEarnings.replace('$', '');
        }else{
            return movieBDetails.imdbRating === movieADetails.imdbRating;
        }          
    }).map(([name,details])=>{
        return {[name]:details};
    });

    return result;
}

console.log(sortMoviesonRating(movies));